package project.aether.aether.net.bt.le

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import project.aether.aether.R

class BtLEScanner constructor(context : Context) {

    private val BLE_UUID = context.getString(R.string.ble_uuid)

    var isScanning = false
    private var scanner: BluetoothLeScanner? = null
    private var mScanCallback: ScanCallback? = null

    private val sFilter: ScanFilter
    private val sSettings : ScanSettings

    private var scanResultCB: ((BluetoothDevice) -> Unit)? = null

    private inner class ScanCallback : android.bluetooth.le.ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            scanResultCB?.invoke(result!!.device)
        }
    }

    init {
        sFilter = ScanFilter.Builder()
            .setServiceUuid(ParcelUuid.fromString(BLE_UUID))
            .build()
        sSettings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()
    }

    fun setScanResultCallback(func: (BluetoothDevice) -> Unit) {
        scanResultCB = func
    }

    fun start() {
        if(isScanning) return

        scanner = scanner?: BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner
        mScanCallback = mScanCallback?: ScanCallback()
        scanner!!.startScan(listOf(sFilter), sSettings, ScanCallback())
    }

    fun stop() {
        scanner?.stopScan(mScanCallback)
    }
}