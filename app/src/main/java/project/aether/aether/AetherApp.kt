package project.aether.aether

import android.app.Application
import android.content.Context

class AetherApp : Application() {
    companion object {
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        AetherApp.context = applicationContext
    }

    fun getContext(): Context {
        return context
    }
}