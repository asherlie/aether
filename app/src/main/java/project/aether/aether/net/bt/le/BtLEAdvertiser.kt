package project.aether.aether.net.bt.le

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import project.aether.aether.R

class BtLEAdvertiser constructor(context: Context) {

    private val BLE_UUID = context.getString(R.string.ble_uuid)

    private val adSettings: AdvertiseSettings
    private val adData: AdvertiseData

    private var isAdvertising = false
    private var advertiser: BluetoothLeAdvertiser? = null
    private var adCallback: AdvertiseCallback = AdvertiseCallback()

    private inner class AdvertiseCallback : android.bluetooth.le.AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            Log.e("BLE", "Advertising onStartFailure: $errorCode")
            super.onStartFailure(errorCode)
            isAdvertising = false
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            Log.e("BLE", "Advertising started")
            super.onStartSuccess(settingsInEffect)
            isAdvertising = true
        }
    }

    init {
        adSettings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
            .setConnectable(false)
            .build()
        adData = AdvertiseData.Builder()
            .addServiceUuid(ParcelUuid.fromString(BLE_UUID))
            .build()
    }

    fun start() {
        if (isAdvertising) return
        BluetoothAdapter.getDefaultAdapter().bluetoothLeAdvertiser.startAdvertising(adSettings, adData, adCallback)
    }

    fun stop() {
        if (!isAdvertising) return
        BluetoothAdapter.getDefaultAdapter().bluetoothLeAdvertiser.stopAdvertising(adCallback)
    }
}