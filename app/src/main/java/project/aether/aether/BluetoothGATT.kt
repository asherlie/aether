package project.aether.aether

import android.bluetooth.*
import android.content.Context
import android.util.Log

class BluetoothGATT constructor(context: Context) {
    companion object {
        @Volatile private var INSTANCE: BluetoothGATT? = null

        fun getInstance(context: Context): BluetoothGATT =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: BluetoothGATT(context.applicationContext).also { INSTANCE = it }
            }
    }

    private var appContext = context

    // Server
    private var gattServerCallback: ServerCallback? = null
    private var gattServer: BluetoothGattServer? = null

    private inner class ServerCallback : BluetoothGattServerCallback() {
        override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
            super.onConnectionStateChange(device, status, newState)
            Log.e("BTGATT", "Device changed $device.address $status $newState")
        }
    }

    fun listen() {
        if (gattServer != null) return
        gattServerCallback = gattServerCallback?: ServerCallback()

        val btManager = appContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        gattServer = btManager.openGattServer(appContext, gattServerCallback)
        Log.e("BTGATT", "Gatt server started")
    }

    // Client
}