package project.aether.aether

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.ParcelUuid
import android.util.Log

class BluetoothLeScanner constructor(context : Context){

    var isScanning = false
    private var scanner: BluetoothLeScanner? = null
    private var mScanCallback: ScanCallback? = null

    private var foundDevices = mutableListOf<String>()

    private inner class ScanCallback : android.bluetooth.le.ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            val deviceAddress = result!!.device.address
            if (!foundDevices.contains(deviceAddress)) {
                Log.e("BLE", "Device found $deviceAddress ($callbackType)")
                foundDevices.add(deviceAddress)
            }

        }
    }
    private val BLE_UUID = context.getString(R.string.ble_uuid)
    private val sFilter: ScanFilter
    private val sSettings : ScanSettings
    init {
            sFilter = ScanFilter.Builder()
                .setServiceUuid(ParcelUuid.fromString(BLE_UUID))
                .build()
        sSettings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()
    }
    fun startScan() {
        if(isScanning) return

        scanner = scanner?: BluetoothAdapter.getDefaultAdapter().bluetoothLeScanner
        mScanCallback = mScanCallback?: ScanCallback()
        scanner!!.startScan(listOf(sFilter), sSettings, ScanCallback())
    }

    fun stopScan() {
        scanner?.stopScan(mScanCallback)
    }
}