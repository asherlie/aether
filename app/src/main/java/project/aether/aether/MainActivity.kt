package project.aether.aether

import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.ParcelUuid
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import project.aether.aether.net.bt.le.BtLEAdvertiser
import project.aether.aether.net.bt.le.BtLEScanner
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

    var mBluetoothAdapter: BluetoothAdapter? = null
    var foundDevices: MutableList<String> = mutableListOf()
    lateinit var bleInstance: BluetoothLE

    private inner class BLECallback : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            if ( callbackType == ScanSettings.CALLBACK_TYPE_FIRST_MATCH ) {
                val deviceAddress = result!!.device.address
                foundDevices.add(deviceAddress)
                Log.e("BLE", "Device found $deviceAddress")
            }
        }
    }

    private inner class GattServerCallback: BluetoothGattServerCallback() {
        override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
            super.onConnectionStateChange(device, status, newState)
            Log.e("GATT", "Connected: ${device!!.address} $status")
        }
    }

    private inner class GattCallback: BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            connectedDevices[gatt!!.device.address] = gatt
            Log.e("GATT", "Gatt connected $newState")
            mHandler.postDelayed({
                gatt.discoverServices()
            }, 1000)
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            Log.e("GATT", "Services discovered $status")
            Log.e("GATT", "Services ${gatt!!.services}")
        }
    }

    val connectedDevices = mutableMapOf<String, BluetoothGatt>()

    val mHandler = Handler()

    val GattService: BluetoothGattService

    init {
        GattService = BluetoothGattService(UUID.fromString("462478a5-ffca-4615-9598-c5bf2e7d8424"),
            BluetoothGattService.SERVICE_TYPE_PRIMARY)

        val dataCharacteristic = BluetoothGattCharacteristic(UUID.fromString("4baec883-8e2f-4ec7-b9f8-deb9a0269161"),
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_NOTIFY,
            BluetoothGattCharacteristic.PERMISSION_READ)

        val dataDescriptor = BluetoothGattDescriptor(UUID.fromString("4baec883-8e2f-4ec7-b9f8-deb9a0269161"),
            BluetoothGattDescriptor.PERMISSION_READ)
        dataCharacteristic.addDescriptor(dataDescriptor)

        GattService.addCharacteristic(dataCharacteristic)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Create our BluetoothLE instance
        val bleAdvertiser = BtLEAdvertiser(applicationContext)
        val bleScanner = BtLEScanner(applicationContext)

        // Get the Multiple Advertisement support flag
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        val mA = mBluetoothAdapter!!.isMultipleAdvertisementSupported

        // Display it for now
        val statusView = this.findViewById<TextView>(R.id.statusView)
        statusView.setText("Multiple Adverisement: $mA")

        // Get buttons
        val hostBtn = this.findViewById<Button>(R.id.hostBtn)
        val clientBtn = this.findViewById<Button>(R.id.clientBtn)

        hostBtn.setOnClickListener {
            Toast.makeText(this, "Host", Toast.LENGTH_SHORT).show()
            bleAdvertiser.start()

            // Make a Gatt server
            val btManager = applicationContext.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val gattServer = btManager.openGattServer(this, GattServerCallback())
            gattServer.addService(GattService)
        }

        clientBtn.setOnClickListener {
            Toast.makeText(this, "Connect", Toast.LENGTH_SHORT).show()

            val seenDevices = mutableListOf<String>()

            bleScanner.setScanResultCallback callback@ {
                if (!seenDevices.contains(it.address)) {
                    seenDevices.add(it.address)
                    Toast.makeText(this, "Found: ${it.address}", Toast.LENGTH_LONG).show()
                    mHandler.post {
                        it.connectGatt(applicationContext, false, GattCallback())
                    }
                }
            }

            bleScanner.start()

            Handler().postDelayed({
                bleScanner.stop()
                Toast.makeText(this, "Stopped Scan", Toast.LENGTH_SHORT).show()
            }, 10000)
        }
    }
}
