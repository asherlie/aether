package project.aether.aether

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.le.*
import android.content.Context
import android.os.ParcelUuid
import android.util.Log

class BluetoothLE constructor(context: Context) {
    companion object {
        @Volatile private var INSTANCE: BluetoothLE? = null

        fun getInstance(context: Context): BluetoothLE =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: BluetoothLE(context.applicationContext).also { INSTANCE = it }
            }
    }

    private var appContext = context


    // Advertising, only supported on some devices
    var isAdvertising = false
    private var advertiser: BluetoothLeAdvertiser? = null
    private var mAdvertiseCallback: AdvertiseCallback? = null

    private inner class AdvertiseCallback : android.bluetooth.le.AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            Log.e("BLE", "Advertising onStartFailure: $errorCode")
            super.onStartFailure(errorCode)
            isAdvertising = false
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            Log.e("BLE", "Advertising started")
            super.onStartSuccess(settingsInEffect)
            isAdvertising = true
        }
    }

    fun canAdvertise(): Boolean {
        return BluetoothAdapter.getDefaultAdapter().isMultipleAdvertisementSupported
    }

    fun startAdvertise() {
        if (!canAdvertise()) return
        if (isAdvertising) return

        advertiser = advertiser?: BluetoothAdapter.getDefaultAdapter().bluetoothLeAdvertiser
        mAdvertiseCallback = mAdvertiseCallback?: AdvertiseCallback()

        val settings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
            .setConnectable(false)
            .build()
        val data = AdvertiseData.Builder()
            .addServiceUuid(ParcelUuid.fromString(appContext.getString(R.string.ble_uuid)))
            .build()
        advertiser!!.startAdvertising(settings, data, AdvertiseCallback())
    }

    fun stopAdvertise() {
        if (!isAdvertising) return
        advertiser?.stopAdvertising(mAdvertiseCallback!!)
        isAdvertising = false
    }
}
    // Scanning
